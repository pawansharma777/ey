import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TargetComponent } from './target/target.component';

const routes: Routes = [
  {
    path: 'target',
    component: HomeComponent
  },
  {
    path: '',
    component: TargetComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
