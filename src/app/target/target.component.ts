import { Component, OnInit } from '@angular/core';
import mockData from '../mockData';
class Target {
  constructor(
    public status: string = '',
    public companyInfo: string = '',
    public keyContacts: string = '',
    public financialPerformance: string = '',
  ) {}
}

@Component({
  selector: 'app-target',
  templateUrl: './target.component.html',
  styleUrls: ['./target.component.css']
})
export class TargetComponent implements OnInit {
  // It maintains list of targets
  targets: Target[] = [];
  // It maintains target Model
  regModel: Target;
  // It maintains target form display status. By default it will be false.
  showNew: Boolean = false;
  // It will be either 'Save' or 'Update' based on operation.
  submitType: string = 'Save';
  // It will apply filter 
  filterTarget: string = '';
  // It maintains table row index based on selection.
  selectedRow: number;
  // It maintains Array of countries.
  targetStatus: string[] = [ 'researching', 'pending approval', 'approved', 'declined'];
  tagetFinance: string[] = [ 'Healthcare', 'Miscellaneous', 'Public Utilities', 'Technology','Finance', 'Energy', 'Others'];
  constructor() {
    // Add default target data.
    this.targets.push(new Target('researching', 'Acacia Research Corporation', 'keyContacts - 1', 'Miscellaneous'));
    this.targets.push(new Target('approved', 'EY', 'keyContacts - 3', 'Technology'));
    this.targets.push(new Target('declined', 'Property Insurance Holdings', 'keyContacts -2 ', 'Finance'));
  }

  ngOnInit() {}

  // This method associate to New Button.
  onNew() {
    // Initiate new target.
    this.regModel = new Target();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display target entry section.
    this.showNew = true;
  }

  // This method associate to Save Button.
  onSave() {
    if (this.submitType === 'Save') {
      // Push target model object into target list.
      this.targets.push(this.regModel);
    } else {
      // Update the existing properties values based on model.
      this.targets[this.selectedRow].status = this.regModel.status;
      this.targets[this.selectedRow].companyInfo = this.regModel.companyInfo;
      this.targets[this.selectedRow].keyContacts = this.regModel.keyContacts;
      this.targets[this.selectedRow].financialPerformance = this.regModel.financialPerformance;
    }
    // Hide target entry section.
    this.showNew = false;
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new target.
    this.regModel = new Target();
    // Retrieve selected target from list and assign to model.
    this.regModel = Object.assign({}, this.targets[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display target entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding target entry from the list.
    this.targets.splice(index, 1);
  }

  // This method associate toCancel Button.
  onCancel() {
    // Hide target entry section.
    this.showNew = false;
  }

  // This method associate to Bootstrap dropdown selection change.
  onChangeStatus(e: any) {
    // Assign corresponding selected country to model.
    this.regModel.status = e.target.value;
  }

  onChangeFilter(e: any) {
    // Assign corresponding selected country to model.
    this.filterTarget = e.target.value;
  }

  onChangeFinance(e: any) {
    // Assign corresponding selected country to model.
    this.regModel.financialPerformance = e.target.value;
  }

}
