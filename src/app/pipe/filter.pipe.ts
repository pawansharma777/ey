import { Injectable, Pipe, PipeTransform } from '@angular/core';  
  
@Pipe({  
    name: 'statusfilter'  
})  
  
@Injectable()  
export class StatusFilterPipe implements PipeTransform {  
    transform(items: any[], value: string): any[] {  
        if (!items) return [];  
        if(value) {  
            return items.filter(item => item.status.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) > -1);  
        }  
        else  
        {  
            return items;  
        }  
    }  
} 